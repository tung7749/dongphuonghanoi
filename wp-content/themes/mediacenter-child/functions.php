<?php
add_action( 'wp_enqueue_scripts', 'enqueue_child_theme_styles', PHP_INT_MAX);
function enqueue_child_theme_styles() {
    
    // Uncomment this section if you want your child's style.css to load.
    /*wp_enqueue_style( 'media_center-main-style', get_template_directory_uri().'/style.css' );
    wp_enqueue_style( 'child-style', get_stylesheet_uri(), array('media_center-main-style')  );*/
    // Uncomment this section to activate RTL from child theme
    /*
    if( is_rtl() ) {
        wp_enqueue_style( 'media_center-rtl-style', get_template_directory_uri().'/rtl.css' );
    }*/
}